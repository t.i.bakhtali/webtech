class Course {
    constructor(name, program, level, semester, descr, teacher, id, photo) {
        this.name = name;
        this.program = program;
        this.level = level;
        this.semester = semester;
        this.description = descr;
        this.teacher = teacher;
        this.id = id;
        this.photo = photo;
    }
}

module.exports = Course;
