#!/usr/bin/env nodejs

var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db = require('./database');

var indexRouter = require('./routes/index');
var courseRouter = require('./routes/courses');
var registerRouter = require('./routes/register');
var loginRouter = require('./routes/login');
var profileRouter = require('./routes/profile');

var app = express();
const port = process.env.PORT || 8008;

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Logging
app.use(logger('dev'));
app.use((req, res, next) => {
    console.log(req.headers);
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}`);
    next();
});

// Session
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ path: '/', httpOnly: true, secure: false, maxAge: null, secret: "session", resave: false, saveUninitialized: false }));

// Routing
/*app.get('/lmao', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', 'test.html'));
});
app.get('/json', (req, res) => {
    res.render('persons', { title: 'People', people: jsonTest });
});
app.get('/json/:id', (req, res) => {
    const found = jsonTest.some(person => person.id === parseInt(req.params.id));
    if (found) {
        res.json(jsonTest.filter(person => person.id === parseInt(req.params.id)));
    } else {
        res.status(400).json({ msg: `Not found with id: ${req.params.id}` });
    }
});*/

app.use('/', indexRouter);
app.use('/courses', courseRouter);
app.use('/register', registerRouter);
app.use('/login', loginRouter);
app.use('/profile', profileRouter);

app.post('/processForm', (req, res) => {
    let name = req.body.name;
    let gender = req.body.gender;
    let salution = (gender === "male") ? "Mr." : "Ms.";
    res.status(200).send("Hello " + salution + " " + name);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

var server = app.listen(port, function() { console.log(`Server Listening on port ${port}`); });

server.on("close", () => {
    db.close();
});
