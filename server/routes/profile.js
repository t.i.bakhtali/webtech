var express = require('express');
var router = express.Router();
var db = require('../database');
var torun = false;
var name;
var password;
var program;
var level;
var registries = [];

router.get('/', function(req, res) {
    var sess = req.session;
    registries = [];
    if (sess.username) {
        db.all(`SELECT coursename FROM Registries WHERE username = ?`, [sess.username], (err, rows) => {
            rows.forEach(row => {
                registries.push(row.coursename);
            });
            return res.render('profile', { title: 'Profile', message: 'Change your attributes here (all at once)', registries: registries });
        });
    } else {
        return res.render('login', { title: 'Login', message: 'Please login first' });
    }
});

router.get('/update', function(req, res) {
    var sess = req.session;

    db.run("UPDATE Students SET name = ?, password = ?, program = ?, level = ? WHERE username = ?", [name, password, program, level, sess.username], (err) => {
        if (err) {
            console.log(err);
            return res.render('profile', { title: 'Profile', message: err, registries: registries });
        } else {
            return res.send("Successfully edited data");
        }
    });

});

router.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return console.log(err);
        }
        res.redirect('/');
    });

});

router.post('/submit', function(req, res) {
    name = req.body.name;
    password = req.body.password;
    program = req.body.program;
    level = req.body.level;
    var sess = req.session;

    db.all(`SELECT coursename, username FROM Registries WHERE username = ?`, [sess.username], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var found = false;

        rows.forEach(row => {
            found = true;
        });

        if (found) {
            db.all(`SELECT level, program FROM Students WHERE username = ?`, [sess.username], (err2, nextrows) => {
                if (err2) {
                    return console.error(err2.message);
                }
                if (nextrows[0].program != program || nextrows[0].level != level) {
                    return res.render('profile', { title: 'Profile', message: 'Still registered to conflicting courses', registries: registries });
                }
            });
        } else {
            res.redirect('/profile/update');
        }
    });
});

module.exports = router;
