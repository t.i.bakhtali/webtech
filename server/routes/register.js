var express = require('express');
var router = express.Router();
var db = require('../database');

router.get('/', function(req, res)
{
  return res.render('register', { title: 'Register'});
});

router.post('/submit', function(req, res)
{
  let name = req.body.name;
  let username = req.body.username;
  let password = req.body.password;
  let program = req.body.program;
  let level = req.body.level;

  db.run("INSERT INTO Students VALUES (?,?,?,?,?)", [name, username, password, program, level], (err) => {
     if (err) {
         console.log(err);
         return res.render('register', { title: 'Register', message: err});
     }
     else {
       var sess=req.session;
       sess.username = username;
       return res.send("Welcome " + name);
     }
  });

});

module.exports = router;
