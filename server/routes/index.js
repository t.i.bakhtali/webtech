var express = require('express');
var router = express.Router();
var message;

/* GET home page. */
router.get('/', function(req, res, next) {
  var sess = req.session;
  var isLoggedIn;
  if(sess.username)
  {
    isLoggedIn = true;
    message = "Welcome " + sess.username;
  }
  else {
    message = "";
  }
    res.render('index', { title: 'Epic Gamer University', isLoggedIn: isLoggedIn, message: message });
});
module.exports = router;
