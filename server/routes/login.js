var express = require('express');
const session = require('express-session');
var router = express.Router();
var db = require('../database');

var app = express();
app.use(session({ path: '/', httpOnly: true, secure: false, maxAge: null, secret: "session", resave: false, saveUninitialized: false }));

router.get('/', function(req, res) {
    var sess = req.session;
    if (sess.username) {
        return res.redirect('/');
    }
    return res.render('login', { title: 'Login' });
});

var username;
var password;

router.post('/submit', function(req, res) {
    username = req.body.username;
    password = req.body.password;

    db.all(`SELECT username, password FROM Students WHERE username = ? AND password = ?`, [username, password], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var found = false;

        rows.forEach(row => {
            found = true;
        });

        if (found) {
            var sess = req.session;
            sess.username = username;
            return res.redirect('/');
        } else {
            return res.render('login', { title: 'Login', message: 'Wrong credentials' });
        }
    });
});

module.exports = router;
