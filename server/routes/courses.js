var express = require('express');
var router = express.Router();
var db = require('../database');
var Course = require('../course');

router.get('/:id', (req, res) => {
    db.all(`SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, C.teacher, T.photo, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.ROWID = ?`, [req.params.id], (err, rows) => {
        if (err) {
            return console.err(err.message);
        }
        var sess = req.session;
        var isLoggedIn;
        var isRegistered = false;
        if (sess.username) {
            isLoggedIn = true;
        } else {
            isLoggedIn = false;
        }
        var row = rows[0];
        var gotCourse = new Course(row.name, row.program, row.level, row.semester, row.descr, row.tname, row.rowid, row.photo);
        db.all(`SELECT * FROM Registries WHERE username = ? AND coursename = ?`, [sess.username, row.name], (errors, results) => {
            if (errors) {
                return console.error(errors.message);
            }
            if (results[0]) {
                isRegistered = true;
            }
            res.render('coursePage', { title: row.name, course: gotCourse, isLoggedIn: isLoggedIn, isRegistered: isRegistered });
        });
    });
});

router.get('/courses/:id', (req, res) => {
    res.redirect('/courses/' + req.params.id);
});

router.get('/', (req, res) => {
    db.all(`SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, C.teacher, T.photo, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id ORDER BY C.program, C.level, C.semester, C.name`, (err, rows) => {
        if (err) {
            return console.error(err.message);
        }

        var programs = [];
        var courses = [];

        rows.forEach(row => {
            courses.push(new Course(row.name, row.program, row.level, row.semester, row.descr, row.tname, row.rowid));
            if (!programs.includes(row.program)) {
                programs.push(row.program);
            }
        });


        return res.render('courses', { title: 'Courses', courses: courses, programs: programs });
    });
});

router.post('/register', (req, res) => {
    var courseprogram;
    var courselevel;
    var coursename;
    var sess = req.session;

    db.all(`SELECT name, program, level FROM Courses WHERE rowid = ?`, [req.body.courseId], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var row = rows[0];
        coursename = row.name;
        courseprogram = row.program;
        courselevel = row.level;

        db.all(`SELECT * FROM Registries WHERE username = ? AND coursename = ?`, [sess.username, coursename], (err, rows2) => {
            if (err) {
                return console.error(err.message);
            }

            if (rows2[0]) {
                res.render('registerError', { title: "Register Failure", cause: "You are already registered for this course!" });
            } else {
                db.all(`SELECT program, level FROM Students WHERE username = ?`, [sess.username], (err, rows) => {
                    if (err) {
                        return console.error(err.message);
                    }

                    studentprogram = rows[0].program;
                    studentlevel = rows[0].level;

                    if (studentlevel != courselevel || studentprogram != courseprogram) {
                        res.render('registerError', { title: "Register Failure", cause: "You are not allowed to register for this course!" });
                    } else {
                        db.all("INSERT INTO Registries VALUES (?,?)", [coursename, sess.username], (err) => {
                            if (err) {
                                console.log(err);
                                return console.error(err.message);
                            } else {
                                console.log("Registered!");
                                return res.render('registerSuccess', { title: "Registration" });
                            }
                        });
                    }
                });
            }
        });
    });
});

router.post('/unregister', (req, res) => {
    var sess = req.session;
    var coursename;

    db.all(`SELECT name FROM Courses WHERE rowid = ?`, [req.body.courseId], (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var row = rows[0];
        coursename = row.name;

        db.all(`DELETE FROM Registries WHERE username = ? AND coursename = ?`, [sess.username, coursename], (err, rows) => {
            if (err) {
                return console.error(err.message);
            }
            return res.render('unregisterSuccess', { title: "Unregistered" });
        })
    });
});

router.post('/search', (req, res) => {
    let name = req.body.name;
    let program = req.body.program;
    let semester = req.body.semester ? req.body.semester : "none";
    let level = req.body.level;
    let skip = false;
    let query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.program = ? AND C.semester = ? AND C.level = ?`;
    let params = [name, program, semester, level];

    if (!skip && (program == "none" && semester == "none" && level == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ?`;
        params = [name];
        skip = true;
    }

    if (!skip && (semester == "none" && level == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.program = ?`;
        params = [name, program];
        skip = true;
    }

    if (!skip && (program == "none" && level == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.semester = ?`;
        params = [name, semester];
        skip = true;
    }

    if (!skip && (level == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.program = ? AND C.semester = ?`;
        params = [name, program, semester];
        skip = true;
    }

    if (!skip && (semester == "none" && program == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.level = ?`;
        params = [name, level];
        skip = true;
    }

    if (!skip && (program == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.semester = ? AND C.level = ?`;
        params = [name, semester, level];
        skip = true;
    }

    if (!skip && (semester == "none")) {
        query = `SELECT C.rowid, C.name, C.program, C.level, C.semester, C.descr, T.name AS tname FROM Courses AS C INNER JOIN Teachers AS T ON C.teacher = T.id WHERE C.name = ? AND C.program = ? AND C.level = ?`;
        params = [name, program, level];
        skip = true;
    }

    db.all(query, params, (err, rows) => {
        if (err) {
            return console.error(err.message);
        }
        var results = [];
        var found = false;
        var id;

        rows.forEach(row => {
            found = true;
            id = row.rowid;
            results.push(new Course(row.name, row.program, row.level, row.semester, row.descr, row.tname));
        });

        if (found) {
            res.redirect('/courses/' + id);
        }

        res.render('resultPage', { terms: [name, program, level, semester], results: results });
    });
});

module.exports = router;