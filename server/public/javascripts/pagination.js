class Pagination{
  constructor(p, c){
    // data needed for pagination
    this.pdata = p;
    this.courses = c;

    // display the first 10 results
    this.showMore();

    // create the button to show more results
    this.buttonHandler = this.showMore.bind(this);
    this.createButton(this.buttonHandler);
  }

  createRow(name, value){
    var row = document.createElement('tr');
    var cname = document.createElement('td');
    var nameNode = document.createTextNode(name);
    cname.classList.add('course__property');
    cname.appendChild(nameNode);
    var cval = document.createElement('td');
    var valNode = document.createTextNode(value);
    cval.classList.add('course__value');
    cval.appendChild(valNode);

    row.appendChild(cname);
    row.appendChild(cval);

    return row;
  }

  // when clicking the button create the next 10 html elements
  showMore(){
    if(this.pdata.current >= this.pdata.total) return;

    this.pdata.current += this.pdata.amount;

    var diff = this.pdata.amount;
    if(this.pdata.current >= this.pdata.total)
      diff = this.pdata.current - this.pdata.total;

    for(var i = this.pdata.current - diff; i < this.pdata.current; i++){
      var course = this.courses[i];

      var table = document.createElement('table');
      table.classList.add('course');
      if(course.level == "BSc") table.classList.add('course--bsc');
      else table.classList.add('course--msc');
      table.appendChild(this.createRow("Course Name: ", course.name));
      table.appendChild(this.createRow("Program: ", course.program));
      table.appendChild(this.createRow("Level: ", course.level));
      table.appendChild(this.createRow("Semester: ", course.semester));
      table.appendChild(this.createRow("Description: ", course.description));
      table.appendChild(this.createRow("Teacher: ", course.teacher));

      var link = document.createElement('a');
      link.href = "courses/" + course.id;
      link.appendChild(table);


      var results = document.getElementById('results');

      var program = document.getElementById(course.program);
      if(program == null){
        program = document.createElement('div');
        program.id = course.program;
        program.classList.add('program');
        program.style.backgroundColor = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
        results.appendChild(program);
      }
      program.appendChild(link);
    }
  }

  createButton(f){
    var button = document.createElement('button');
    button.classList.add('pagination');
    button.addEventListener('click', f);
    var text = document.createTextNode("More");
    button.appendChild(text);
    document.body.appendChild(button);
  }
}
