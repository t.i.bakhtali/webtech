var fs = require("fs");

var file = __dirname + "/db/test.db";
if (!fs.existsSync(file)) {
    fs.openSync(file, "w");
}

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

/*db.serialize(() => {

    // Create Tables
    db.run("CREATE TABLE IF NOT EXISTS Courses (name TEXT, program TEXT, level TEXT, semester INT, descr TEXT, teacher INT, UNIQUE(name), FOREIGN KEY (teacher) REFERENCES Teachers (name))");
    db.run("CREATE TABLE IF NOT EXISTS Teachers (name TEXT, photo TEXT, UNIQUE(name))");
    db.run("CREATE TABLE IF NOT EXISTS Students (name TEXT, username TEXT, password TEXT, program TEXT, level TEXT, UNIQUE(name))");

    /*db.get(`SELECT name name FROM Courses WHERE name = ?`, ["Databases"], (err, row) => {
        if (err) {
            return console.error(err.message);
        }

        return row ? console.log(`${row.name}`) : console.log(`None Found`);
    });
    var stmt = db.prepare("INSERT INTO Courses VALUES (?, ?, ?, ?, ?, ?)");
    //stmt.run("Databases", "Informatica", "BSc", 3, "Tables and Shit", 1);
    //stmt.finalize();
});*/

module.exports = db;