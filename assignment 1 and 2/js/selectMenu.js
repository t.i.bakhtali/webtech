var elementPicker = document.createElement("select");
elementPicker.classList.add("header__menu");

var colorPicker = document.createElement("select");
colorPicker.classList.add("header__menu");

var fontSizePicker = document.createElement("select");
fontSizePicker.classList.add("header__menu");

var selectButton = document.createElement("button");
selectButton.classList.add("header__button");

var dropDownItems = [
    [document.getElementsByTagName("body"), "Body"],
    [document.getElementsByTagName("header"), "Header"],
    [document.getElementsByTagName("footer"), "Footer"],
    [document.getElementsByTagName("aside"), "Aside"],
    [document.getElementsByTagName("article"), "Article"],
    [document.getElementsByTagName("section"), "Section"]
];

var elementList = [];

for (let i = 0; i < dropDownItems.length; i++) {
    [collection, tag] = dropDownItems[i];
    addToDropDown(collection, tag);
}

function addToDropDown(tagCollection, tagType) {
    for (let i = 0; i < tagCollection.length; i++) {
        var option = document.createElement("option");
        if (i == 0) {
            option.text = tagType;
        } else {
            option.text = tagType + " " + i.toString();
        }
        elementList.push(tagCollection[i]);
        elementPicker.add(option);
    }
}

function makeSelect(collection, menu) {
    for (let i = 0; i < collection.length; i++) {
        var item = document.createElement("option");
        item.text = collection[i];
        menu.add(item);
    }

}

makeSelect(["Red", "Blue", "Green"], colorPicker);
makeSelect(["8px", "16px", "24px"], fontSizePicker);

selectButton.addEventListener("click", function() {
    elementList[elementPicker.selectedIndex].style.backgroundColor = colorPicker.value;
    elementList[elementPicker.selectedIndex].style.fontSize = fontSizePicker.value;


});
header = document.getElementsByTagName("header")[0];
selectWrapper = document.createElement("select__wrapper");
header.appendChild(document.createElement("p"));
header.appendChild(selectWrapper);
selectWrapper.appendChild(elementPicker);
selectWrapper.appendChild(colorPicker);
selectWrapper.appendChild(fontSizePicker);
selectButton.appendChild(document.createTextNode("Apply"));
selectWrapper.appendChild(selectButton);