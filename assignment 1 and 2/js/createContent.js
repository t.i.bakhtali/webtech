var header = document.getElementById('header');
var textNode = document.createTextNode("An Intro To Haskell");
header.appendChild(textNode);


var nav = document.getElementById('nav');
var ul = document.createElement('ul');
nav.appendChild(ul);
ul.classList.add('menu');

var menuItems = [["Home", '../index.html'], ["Schedule", 'schedule.html'], ["Staff", 'staff.html'], ["Assignments", 'assignments.html'], ["About", 'about.html'], ["Info", 'info.html']];

for(var i = 0; i < menuItems.length; i++){
  [text, link] = menuItems[i];
  menuItem(ul, text, link);
}

function menuItem(ul, text, link){
  var a = document.createElement('a');
  a.classList.add('menu__item');
  if (text == "Info")
  {
	a.classList.add('current');
  }
  a.href = link;
  var li = document.createElement('li');
  var textNode = document.createTextNode(text);
  a.appendChild(textNode);
  li.appendChild(a);
  ul.appendChild(li);
}

var main = document.getElementById('main');
var section = document.createElement('section');
main.appendChild(section);
var wrapper = document.createElement('div');
section.appendChild(wrapper);
section.classList.add('content');
wrapper.classList.add('content__wrapper');



subject = new Subject("Intro To Haskel", "B0O8", "69", "D", "../index.html");
var participants = "420";
var timetable = "The offical schedule can be found in MyTimetable";

teacherRobbie = new Professor("Robbie", "B0O8", "BBG-569");
teacherStephan = new Professor("Stephan", "B0O8", "BBG-420");
teacherTariq = new Professor("Tariq", "B0O8", "BBG-666");

taQmonLmao = new Assistant("Qmon Lmao", "B0O8", 1);
taRonadMcDonald = new Assistant("Ronald McDonald", "B0O8", 1);
taNumber15 = new Assistant("Number 15", "B0O8", 2);

colleges = new Colleges([teacherRobbie, teacherStephan, teacherTariq], [taQmonLmao, taRonadMcDonald, taNumber15]);

var content = `This course aims to teach you how to program in the beautiful langauge called Haskell.
  Haskell is a Functional Programming language, which means it is entirely based on expressions and functions.
  This is particularly useful for the compiler, because we can reason about our programs.
  In object-oriented languages, you cannot do this because the code is impure.
  Purity in terms of code means that it either will, or will not produce side-effects.
  Side-effects are operations like I/O actions, for example printing or reading from the console.
  Some important topics in this course are higher-order functions, polymorphism in Functional Programming, data types, and pattern matching.
  Haskell features a lot of abstractions like Monads, Monoids, and Functors.
  By the end of the course we hope you can write idiomatic Haskell code, as well as understand and apply Functional Programming concepts.`

var learnHaskell = document.createElement('a');
learnHaskell.href = "http://learnyouahaskell.com/";
learnHaskell.appendChild(document.createTextNode("Learn You A Haskell"));

var officalWebsite = document.createElement('a');
officalWebsite.href = "https://www.haskell.org";
officalWebsite.appendChild(document.createTextNode("Offical Website"));

var wiki = document.createElement('a');
wiki.href = "https://en.wikipedia.org/wiki/Haskell_(programming_language)";
wiki.appendChild(document.createTextNode("Wikipedia"));


var literature = [ ["Recommended Reading", [learnHaskell]], ["Other Sources", [officalWebsite, wiki]] ];

var grading = [
  ["The Final Grade will depend on the exams and the three assignments",
    ["The theory grade, T, is 0.4 x mid-term grade + 0.6 x final exam grade. You will need at least T >= 5 to pass the course."
    , "The practical grade, P, is 0.3 x first assignment + 0.3 x second assignment + 0.4 x third assignment. You will need to pass at least two out of three assignments and obtain P >= 5 to pass the course."
    ]
  ],
  ["The Final Grade is computed as 0.5 x T + 0.5 x P. The final result of the course is:",
    ["Passed with grade F if F >= 5.5, T >= 5, P >= 5 and passed at least two out of three lab assignments"
    ,"Not passed with AANV if F > 4.0, but T < 5 or P < 5 or passed fewer than two lab assignments"
    ,"Not passed with NVD otherwise"
    ]
  ]
];

description = new Description(content, literature, grading);

course = new Course(wrapper, subject, participants, timetable, colleges, description);

course.create();

var elem = document.getElementsByClassName("colleges-table")[0];
var descendants = elem.querySelectorAll("*")
for (i = 0; i < descendants.length; i++)
{	
	descendants[i].addEventListener("mouseout", function(event) { event.currentTarget.style.backgroundColor = "white";}, true);
	descendants[i].addEventListener("mouseover", function(event) { if(event.currentTarget.tagName=="TH" || event.currentTarget.tagName=="TD"){event.stopImmediatePropagation();} event.currentTarget.style.backgroundColor = "yellow";}, false);
}