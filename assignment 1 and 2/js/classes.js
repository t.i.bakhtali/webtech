class Teacher {
  constructor(name, subject){
    this.name = name;
    this.subject = subject;
  }
}

class Professor extends Teacher {
  constructor(name, subject, room){
    super(name, subject);
    this.room = room;
  }
}

class Assistant extends Teacher {
  constructor(name, subject, group){
    super(name, subject);
    this.group = group;
  }
}

function createTableHeader(headers){
  var thead = document.createElement('thead');
  var tr = document.createElement('tr');
  thead.appendChild(tr);

  for (var i = 0; i < headers.length; i++){
    var td = document.createElement('th');
    var text = document.createTextNode(headers[i]);
    td.appendChild(text);
    tr.appendChild(td);
  }
  return thead;
}

class Colleges {
  constructor(teachers, assistants){
    this.teachers = teachers;
    this.assistants = assistants;
  }

  create() {
    var teachersLength = this.teachers.length;
    var assistantsLength = this.assistants.length;
    var groupLength = 0;
    for(var i = 0; i < assistantsLength; i++){
      if (this.assistants[i].group > groupLength){ groupLength = this.assistants[i].group; }
    }

    var table = document.createElement('table');
    table.classList.add("colleges-table");
    var thead = createTableHeader(["type", "group", "teacher"]);
    table.appendChild(thead);
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);

    var collegeRow = document.createElement('tr');
    tbody.appendChild(collegeRow);
    var collegeType = document.createElement('td');
    collegeType.appendChild(document.createTextNode("College"));
    collegeRow.appendChild(collegeType);
    collegeRow.appendChild(document.createElement('td')); // empty column
    var collegeTeachers = document.createElement('td');
    collegeRow.appendChild(collegeTeachers);

    for(var i = 0; i < teachersLength; i++){
      var li = document.createElement('li');
      var a = document.createElement('a');
      a.href = "staff.html";
      li.appendChild(a);
      var name = document.createTextNode(this.teachers[i].name);
      a.appendChild(name);
      collegeTeachers.appendChild(li);
    }

    for(var i = 0; i < groupLength; i++){
      var row = document.createElement('tr');
      tbody.appendChild(row);
      if (i == 0) {
        var type = document.createElement('td');
        row.appendChild(type);
        type.setAttribute('rowspan', groupLength);
        type.appendChild(document.createTextNode("Workcollege"));
      }
      var group = document.createElement('td');
      group.appendChild(document.createTextNode("group " + (i + 1)));
      row.appendChild(group);
      var taCell = document.createElement('td');
      row.appendChild(taCell);

      for(var j = 0; j < assistantsLength; j++){
        if(this.assistants[j].group == i + 1){
          var li = document.createElement('li');
          var name = document.createTextNode(this.assistants[j].name);
          li.appendChild(name);
          taCell.appendChild(li);
        }
      }
    }

    return table;
  }
}

class Subject {
  constructor(title, code, ect, timeslot, website){
    this.title = title;
    this.code = code;
    this.ect = ect;
    this.timeslot = timeslot;
    this.website = website;
  }
}

class Description {
  constructor(content, literature, grading){
    this.content = content;
    this.literature = literature;
    this.grading = grading;
  }
}

function createList(list){
  var section = document.createElement('section');
  for(var i = 0; i < list.length; i++){
    [text, items] = list[i];
    var p = document.createElement('p');
    p.appendChild(document.createTextNode(text));
    var ul = document.createElement('ul');
    ul.classList.add('list');
    p.appendChild(ul);
    section.appendChild(p);

    for(var j = 0; j < items.length; j++){
      var li = document.createElement('li');
      if(typeof items[j] === 'string')
        li.appendChild(document.createTextNode(items[j]));
      else
        li.appendChild(items[j]);
      ul.appendChild(li);
    }
  }
  return section;
}

class Course {
  constructor(hook, subject, participants, timetable, colleges, description){
    this.hook = hook;
    this.subject = subject;
    this.participants = participants;
    this.timetable = timetable;
    this.colleges = colleges;
    this.description = description;
  }

  createRow(name, content){
    var row = document.createElement('tr');
    var col1 = document.createElement('td');
    var rowName = document.createTextNode(name);
    var italics = document.createElement('i');
    italics.appendChild(rowName);
    col1.appendChild(italics);
    var col2 = document.createElement('td');
    if(typeof content === 'string' || content instanceof String)
      col2.appendChild(document.createTextNode(content));
    else
      col2.appendChild(content);
    row.appendChild(col1);
    row.appendChild(col2);
    return row;
  }

  create(){
    var h1 = document.createElement('h1');
    var b = document.createElement('b');
    var title = document.createTextNode(this.subject.title);
    b.appendChild(title);
    h1.appendChild(b);
    this.hook.appendChild(h1);
    var table = document.createElement('table');
    table.classList.add("info-table");
    this.hook.appendChild(table);

    var website = document.createElement('a');
    website.href = this.subject.website;
    website.appendChild(document.createTextNode("website with more information"));
    table.appendChild(this.createRow("Website", website));
    table.appendChild(this.createRow("Code", this.subject.code));
    table.appendChild(this.createRow("Ect", this.subject.ect));
    table.appendChild(this.createRow("Timeslot", this.subject.timeslot));
    table.appendChild(this.createRow("Participants",  this.participants));
    table.appendChild(this.createRow("Timetable", this.timetable));
    var collegesTable = this.colleges.create();
    table.appendChild(this.createRow("Teachers", collegesTable));
    table.appendChild(this.createRow("Content", this.description.content));
    var lit = createList(this.description.literature);
    table.appendChild(this.createRow("Literature", lit));
    var grading = createList(this.description.grading);
    table.appendChild(this.createRow("Grading", grading));
  }
}
